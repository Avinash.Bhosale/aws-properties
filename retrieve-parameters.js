// ES6+ example
const { SSMClient, GetParametersByPathCommand, GetParametersByPathCommandInput, CreateOpsItemResponse } = require("@aws-sdk/client-ssm");
const client = new SSMClient({ region: "us-east-1" });
const fs = require('fs');
let path = ['/api/appointment', '/api/appointments', '/api/patient/appointment'];
// let path = ['/api/profile/auth'];

let parameters = [];
async function getParameters(pathPrefix) {
    var params = {
        Path: pathPrefix,
        Recursive: true
    };
    let command = new GetParametersByPathCommand(params);
    let response = await client.send(command);
    let nextToken = response.NextToken;
    console.log(response.parameters);
    pushParameters(response.Parameters);
    console.log(`Next token: ${nextToken} ${parameters.length}`);
    if (nextToken) {
        console.log('Calling NExt Page');
        await getParametersUsingToken(pathPrefix, nextToken)
    }
}
function pushParameters(responseParameters) {
    if(responseParameters) {
        responseParameters.forEach(element => {
            parameters.push(element);
        });    
    }
}
async function getParametersUsingToken(pathPrefix, token) {
    var params = {
        Path: pathPrefix,
        NextToken: token,
        Recursive: true
    };
    console.log('Getting Next Page');
    let command = new GetParametersByPathCommand(params);
    let response = await client.send(command);
    pushParameters(response.Parameters);
    console.log(response.parameters);
    let nextToken = response.NextToken;
    console.log(`Next token: ${nextToken} ${parameters.length}`);
    if (nextToken) {
        console.log('Getting Next Page');
        await getParametersUsingToken(pathPrefix, nextToken)
    }
}
async function processPathPrefixes() {
    let unresolvedPromises = path.map(element => getParameters(element));
    await Promise.all(unresolvedPromises);
}
processPathPrefixes().then((data) => {
        console.log(`Length: ${parameters.length}`);

        try {
            let content = JSON.stringify(parameters, null, 4);
            const f = fs.writeFileSync('parameters.json', content);
            //file written successfully
        } catch (err) {
            console.error(err)
        }
    }, 
    (err) => {
        console.log(err);
});
