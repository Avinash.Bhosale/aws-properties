const { SecretsManagerClient, ListSecretsCommand, GetSecretValueCommand } = require("@aws-sdk/client-secrets-manager");
const fs = require('fs');
const client = new SecretsManagerClient({ region: "us-east-1" });
let secrets = [];
let path = ['/api/appointment', '/api/appointments', '/api/patient/appointment'];
// let path = ['/api/profile/auth']
async function getSecretValuesFromList(secretList) {
    await secretList.map(async (secretItem) => {
        console.log(secretItem.Name);
        let getSecretValueInput = {
            SecretId: secretItem.Name
        };
        let getSecretValueCommand = new GetSecretValueCommand(getSecretValueInput);
        let secretValueResponse = await client.send(getSecretValueCommand);
        console.log('Retrieved Secret', secretValueResponse.Name);
        secrets.push(secretValueResponse);
    });
}
async function getSecretValue(secretItem) {
    console.log(secretItem.Name);
    let getSecretValueInput = {
        SecretId: secretItem.Name
    };
    let getSecretValueCommand = new GetSecretValueCommand(getSecretValueInput);
    let secretValueResponse = await client.send(getSecretValueCommand);
    console.log('Retrieved Secret', secretValueResponse.Name);
    secrets.push(secretValueResponse);
}

async function processSecretPath() {
    let filter = {
        Key: 'name',
        Values: path
    };
    let input = {
        Filters: [filter]
    }
    const command = new ListSecretsCommand(input);
    let secretListOutput = await client.send(command);
    let secretList = secretListOutput.SecretList;
    for (let i=0; i<secretList.length; i++) {
        await getSecretValue(secretList[i]);
    }
}
processSecretPath().then(async (data) => {
    console.log(`Writing Secrets ${secrets.length}`);
    let content = JSON.stringify(secrets, null, 4);
    try {
        const f = fs.writeFileSync('secrets.json', content);
    } catch(err) {
        console.error(err);
    }
},
(err) => {
    console.log(err)
});
