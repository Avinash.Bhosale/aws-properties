const { SecretsManagerClient, CreateSecretCommand } = require("@aws-sdk/client-secrets-manager");
const fs = require('fs');
let rawData = fs.readFileSync('secrets.json');
let secrets = JSON.parse(rawData);
const client = new SecretsManagerClient({ region: "us-east-1" });
async function createSecret(secretName, secretString) {
    let input = {
        Name: secretName,
        SecretString: secretString
    };
    const command = new CreateSecretCommand(input);
    const response = await client.send(command);
}
async function createAllSecrets() {
    let unresolvedPromises = secrets.map(element => createSecret(element.Name, element.SecretString));
    await Promise.all(unresolvedPromises);
}
createAllSecrets().then((data) => {
    console.log("All Secrets Created Successfully");
}, (err) => {
    console.error(err);
});
