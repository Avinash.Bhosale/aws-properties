const { SSMClient, PutParameterCommand } = require("@aws-sdk/client-ssm");
const fs = require('fs');
let rawdata = fs.readFileSync('parameters.json');
let parameters = JSON.parse(rawdata);
// parameters.forEach(parameter => console.log(parameter.Name));
const client = new SSMClient({ region: "us-east-1" });
async function putParameter(parameterName, parameterValue, parameterType) {
    let input = {
        Name: parameterName,
        Value: parameterValue,
        Type: parameterType
    }
    console.log('Putting Parameter', input);
    const command = new PutParameterCommand(input);
    const response = await client.send(command);
}
async function putAllParameters() {
    let unresolvedPromises = parameters.map(element => putParameter(element.Name, element.Value, element.Type));
    await Promise.all(unresolvedPromises);
}
putAllParameters().then((data) => {
    console.log("All Parameters Put Successfully");
}, (err) => {
    console.error(err);
});
